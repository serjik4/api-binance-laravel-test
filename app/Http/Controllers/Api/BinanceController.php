<?php

namespace App\Http\Controllers\Api;

use Binance\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class BinanceController extends Controller
{
    public function getFunds()
    {
        $binance = new API('9qY3knh6GjAxLvgC45vLJkFpVxHvQveXAiQJZ7uGk3V970jesvIl8933olGcOmNF', 'WqwXTQPJXs2FFkH0POuCEM2508u0rF8SYh71yTkI5tW3wGpWA4lx2hKdNCBmVotC');

        $binance->useServerTime();
        $balances = $binance->balances();

        $data = [];
        $total = 0;
        foreach ($balances as $symbol => $info)
        {
            if($symbol != 'BTC' && $info['available'] != 0)
            {
                $data[$symbol] = [];
                $data[$symbol]['price'] = $this->getUSDTPrice($binance, $symbol);
                $data[$symbol]['quantity'] = 0;
                if ($binance->history($symbol . 'BTC'))
                {
                    foreach ($binance->history($symbol . 'BTC') as $key => $value)
                    {
                        if($value['isBuyer'])
                        {
                            $data[$symbol]['quantity'] += $value['qty'];
                        } else {
                            $data[$symbol]['quantity'] -= $value['qty'];
                        }
                    }
                    $data[$symbol]['total'] = round($data[$symbol]['price'] * $data[$symbol]['quantity'], 4);
                    $total += $data[$symbol]['total'];
                } else {
                    $data[$symbol]['quantity'] += $info['available'];
                    $data[$symbol]['total'] = round($data[$symbol]['price'] * $data[$symbol]['quantity'], 4);
                    $total += $data[$symbol]['total'];
                }
            } elseif($symbol == 'BTC' && $info['available'] != 0) {
                $data[$symbol]['price'] = $this->getUSDTPrice($binance, $symbol);
                $data[$symbol]['quantity'] = $info['available'];
                $data[$symbol]['total'] = round($data[$symbol]['price'] * $data[$symbol]['quantity'], 4);
                $total += $data[$symbol]['total'];
            }
        }
//        dd($data);

        return view('welcome', [
            'data' => $data,
            'total' => round($total, 4)
        ]);

    }
    /*public function getFunds()
    {
        $binance = new API('9qY3knh6GjAxLvgC45vLJkFpVxHvQveXAiQJZ7uGk3V970jesvIl8933olGcOmNF', 'WqwXTQPJXs2FFkH0POuCEM2508u0rF8SYh71yTkI5tW3wGpWA4lx2hKdNCBmVotC');
        $binance->useServerTime();
        $balances = $binance->account();
        $data = [];
        $total = 0;
        foreach ($balances['balances'] as $key => $currency) {
            if ($currency['free'] != 0) {
                $currency['price'] = $this->getUSDTPrice($binance, $currency['asset']);
                $currency['total'] = round((float)$currency['free'] * (float)$currency['price'], 4);
                $data[] = $currency;
                $total += $currency['total'];
            }
        }
        return view('welcome', [
            'data' => $data,
            'total' => $total
        ]);

    }*/

    public function getUSDTPrice(API $binance, $symbol)
    {
        $btc = $binance->price('BTCUSDT');
        if($symbol != 'BTC')
        {
            return round($binance->price($symbol . 'BTC') * $btc, 4);
        }
        return round($btc, 4);
    }
}
